#include "pch.h"
#include <iostream>
#include <random>
#include <algorithm>
#include <cmath>

#include "Game.h"

using namespace std;

Game::Game()
{
	initialize();
}


Game::~Game()
{
}

void Game::initialize() {
	board.initialize();
	turn_num = 1;
	turn = true;
}

//bitの位置に着手出来たらboolを返し着手する,出来なければfalseを返す
bool Game::move(ull bit) {
	ull black, white, rev;
	if (bit == 0)
		return false;
	if (turn) {
		black = board.get_black();
		white = board.get_white();
	}
	else {
		black = board.get_white();
		white = board.get_black();
	}
	rev = reverse(black, white, bit);
	if (rev == 0)
		return false;
	if (turn) {
		board.xor_black(rev | bit);
		board.xor_white(rev);
	}
	else {
		board.xor_white(rev | bit);
		board.xor_black(rev);
	}
	return true;
}

//black視点でbitに着手した際に裏返るビットを返す
ull Game::reverse(ull black, ull white, ull bit) {
	ull rev = 0,ans=0,mask=0;
	if (((black | white) & bit) != 0)
		return rev;
	mask = trans_E(bit);
	while ((mask != 0) and ((mask & white) != 0)) {
		rev = rev | mask;
		mask = trans_E(mask);
	}
	if ((mask&black) != 0)
		ans = ans | rev;
	mask = trans_W(bit);
	rev = 0;
	while ((mask != 0 and (mask & white) != 0)) {
		rev = rev | mask;
		mask = trans_W(mask);
	}
	if ((mask & black) != 0)
		ans = ans | rev;
	mask = trans_N(bit);
	rev = 0;
	while ((mask != 0 and (mask & white) != 0)) {
		rev = rev | mask;
		mask = trans_N(mask);
	}
	if ((mask&black) != 0)
		ans = ans | rev;
	mask = trans_S(bit);
	rev = 0;
	while ((mask != 0 and (mask & white) != 0)) {
		rev = rev | mask;
		mask = trans_S(mask);
	}
	if ((mask & black) != 0)
		ans = ans | rev;
	mask = trans_NE(bit);
	rev = 0;
	while ((mask != 0 and (mask & white) != 0)) {
		rev = rev | mask;
		mask = trans_NE(mask);
	}
	if ((mask&black) != 0)
		ans = ans | rev;
	mask = trans_SE(bit);
	rev = 0;
	while ((mask != 0 and (mask & white) != 0)) {
		rev = rev | mask;
		mask = trans_SE(mask);
	}
	if ((mask & black) != 0)
		ans = ans | rev;
	mask = trans_SW(bit);
	rev = 0;
	while ((mask != 0 and (mask & white) != 0)) {
		rev = rev | mask;
		mask = trans_SW(mask);
	}
	if ((mask&black) != 0)
		ans = ans | rev;
	mask = trans_NW(bit);
	rev = 0;
	while ((mask != 0 and (mask & white) != 0)) {
		rev = rev | mask;
		mask = trans_NW(mask);
	}
	if ((mask & black) != 0)
		ans = ans | rev;
	return ans;
}

//着手できるマスに1が立っているbit列を返す
ull Game::has_moves() {//合法主があればTrue, なければFalseを返す
	ull bit = 1,black,white,moves=0;
	
	if (turn) {
		black = board.get_black();
		white = board.get_white();
	}
	else {
		white = board.get_black();
		black = board.get_white();
	}
	while (bit) {
		if (reverse(black, white, bit) != 0)
			moves = moves | bit;
		bit = bit << 1;
	}
	return moves;
}

//合法手があればfalse,black,white共に合法手が無ければtrueを返す
bool Game::is_end()

{ //trueならゲーム終了,falseならゲーム中
	if (has_moves() == 0) { //合法手があればゲーム中，なければゲーム終了
		change_turn();
		if (has_moves() == 0) {
			change_turn();
			return true;
		}
		else {
			change_turn();
			return false;
		}
	}
	return false;

}

//ランダムな着手を返す(着手はしない)
ull Game::get_random_move() {
	ull moves = has_moves();
	if (moves == 0)
		return 0;
	vector<ull> list;
	while (moves!=0) {
		ull move = get_first_1_bit(moves);
		moves = moves ^ move;
		list.push_back(move);
	}
	random_device get_rand_dev;
	mt19937_64 get_rand_mt(get_rand_dev());
	shuffle(list.begin(), list.end(),get_rand_mt);
	return list[0];
}

//black視点での評価値を返す,正の値ならblack,負の値ならwhiteが有利
int Game::evaluate(){
	int score = 0;
	if (has_moves() == 0) {
		change_turn();
		if (has_moves() == 0) {
			change_turn();
			score = count_black() - count_white();
			if (score > 0)
				return 1000 + score;
			else if (score < 0)
				return -1000 + score;
			else
				return 0;
		}
		else
			change_turn();
	}
	ull black = board.get_black();
	short bit = 0;
	while (black != 0) {
		bit = get_first_1_num(black)-1;
		score += eva[bit];
		black = black - (black &(-(long long)black));
	}
	ull white = board.get_white();
	while (white != 0) {
		bit = get_first_1_num(white)-1;
		score -= eva[bit];
		white = white - (white &(-(long long)white));
	}
	return score;
}

//depth手を読んで最も評価値の高い手とその評価値のpairを返す
pair<int,ull> Game::search(short depth) {
	DEBUG("searchだよ");
	vector<pair<int, ull>> moves = search_kyoutuu(depth);
	/*
	if (depth == DEPTH) {

		cout << "sort前だよ" << endl;;
		for (int i = 0; i < (int)moves.size(); i++) {
			cout << "評価値:" << moves[i].first << " 手:";
			print_bit(moves[i].second);
		}
	}*/
	DEBUG("depth:" << depth);
	sort(moves.begin(), moves.end());
	/*
	if (depth == blDEPTH) {
		cout << "sort後だよ" << endl;
		for (int i = 0; i < (int)moves.size(); i++) {
			cout << "評価値:" << moves[i].first << " 手:";
			cout << get_first_1_num(moves[i].second) << endl;
			//print_bit(moves[i].second);
		}
		cout << "この手を返したよ" << endl;
	}
	*/
	if (get_turn()) {
		/*
		if (depth == blDEPTH)
			cout << "評価値:" << moves[moves.size() - 1].first << "手;" << get_first_1_num(moves[moves.size() - 1].second) << endl;*/
		return moves[moves.size() - 1];
	}
	else {
		/*
		if (depth == blDEPTH)
			cout << "評価値:" << moves[0].first << "手;" << get_first_1_num(moves[0].second) << endl;*/
		return moves[0];
	}
}
//depth手を読んで最も評価値の低い手とその評価値のpairを返す
pair<int,ull> Game::search_yowa(short depth) {
	DEBUG("searchだよ");
	vector<pair<int, ull>> moves = search_kyoutuu(depth);
	/*
	if (depth == DEPTH) {

		cout << "sort前だよ" << endl;;
		for (int i = 0; i < (int)moves.size(); i++) {
			cout << "評価値:" << moves[i].first << " 手:";
			print_bit(moves[i].second);
		}
	}*/
	DEBUG("depth:" << depth);
	sort(moves.begin(), moves.end());
	/*
	if (depth == DEPTH) {
		cout << "sort後だよ" << endl;
		for (int i = 0; i < (int)moves.size(); i++) {
			cout << "評価値:" << moves[i].first << " 手:";
			print_bit(moves[i].second);
		}
	}*/
	if (!get_turn())
		return moves[moves.size() - 1];
	else
		return moves[0];
}

vector<pair<int, ull>> Game::search_kyoutuu(short depth) {
	vector<pair<int, ull>> list;
	ull moves = has_moves(), mv, bit, black, white, rev;
	if (moves == 0) {
		list.push_back(make_pair(evaluate(), 0));
		return list;
	}
	while (moves) {
		mv = get_first_1_bit(moves);
		moves = moves ^ mv;
		bit = 1;
		if (turn) {
			black = board.get_black();
			white = board.get_white();
		}
		else {
			white = board.get_black();
			black = board.get_white();
		}
		rev = reverse(black, white, mv);
		if (turn) {
			board.xor_black(rev | mv);
			board.xor_white(rev);
			change_turn();
			if (depth > 0) {
				DEBUG("depth-1にして続けるよblackだよ");
				list.push_back(make_pair(search(depth - 1).first, mv));
			}
			else {
				DEBUG("depth=0だよblackだよ");
				list.push_back(make_pair(evaluate(), mv));
			}
			board.xor_black(rev | mv);
			board.xor_white(rev);
			change_turn();
		}
		else {
			board.xor_white(rev | bit);
			board.xor_black(rev);
			change_turn();
			if (depth > 0) {
				DEBUG("depth-1にして続けるよwhiteだよ");
				list.push_back(make_pair(search(depth - 1).first, mv));
			}
			else {
				DEBUG("depth=0だよwahiteだよ");
				list.push_back(make_pair(evaluate(), mv));
			}
			board.xor_white(rev | bit);
			board.xor_black(rev);
			change_turn();
		}
	}/*
	for (int i = 0; i < (int)list.size(); i++) {
		cout << "評価値:" << list[i].first << " 手:" << get_first_1_num(list[i].second) << endl;
	}*/
	return list;
}

ull Game::get_rantuyo_move(short depth) {
	vector<pair<int, ull>> moves = search_kyoutuu(depth);
	/*
	for (int i = 0; i < (int)moves.size(); i++) {
		cout << "評価値:" << moves[i].first << " 手:" << get_first_1_num(moves[i].second) << endl;
	}*/
	vector<ull> ans;
	sort(moves.begin(), moves.end());
	for (int i = 0; i < (int)moves.size(); i++) {
		for (int j = 0; j < pow(2, i); j++) {
			ans.push_back(moves[i].second);
			//cout << "評価値:" << moves[i].first << " 手:" << get_first_1_num(moves[i].second) << endl;
		}
	}
	random_device get_rand_dev;
	mt19937_64 get_rand_mt(get_rand_dev());
	shuffle(ans.begin(), ans.end(), get_rand_mt);
	return ans[0];
}

ull Game::get_ranyowa_move(short depth) {
	vector<pair<int, ull>> moves = search_kyoutuu(depth);/*
	cout << "元の配列だよ" << endl;
	for (int i = 0; i < (int)moves.size(); i++) {
		cout << "評価値:" << moves[i].first << " 手:" << get_first_1_num(moves[i].second) << endl;
	}
	cout << "変えた後の配列だよ" << endl;*/
	vector<ull> ans;
	sort(moves.begin(), moves.end());
	for (int i = 0; i < (int)moves.size(); i++) {
		for (int j = 0; j < pow(2, i); j++) {
			ans.push_back(moves[moves.size() - i - 1].second);
			//cout << "評価値:" << moves[moves.size() - i - 1].first << " 手:" << get_first_1_num(moves[moves.size() - i - 1].second) << endl;
		}
	}
	random_device get_rand_dev;
	mt19937_64 get_rand_mt(get_rand_dev());
	shuffle(ans.begin(), ans.end(), get_rand_mt);
	return ans[0];
}

int Game::hum_eva(ull move){
	vector<pair<int,ull>> moves = search_kyoutuu(s_depth);
	sort(moves.begin(), moves.end());
	int eva = moves[moves.size() / 2].first;
	for (int i = 0; i < (int)moves.size(); i++) {
		if (moves[i].second == move) {;
			eva = moves[i].first;
			break;
		}
	}
	return get_turn() ? (eva - moves[moves.size() / 2].first) : -(eva - moves[moves.size() / 2].first);
}