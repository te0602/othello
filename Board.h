#pragma once

#include <string>

#include "Header.h"

class Board
{
private:
	ull black;
	ull white;
	bool hint = false;
public:
	Board();
	~Board();
	void initialize() {
		hint = false;
		black_init();
		white_init();
	}
	void black_init() {
		black = 0x0000000810000000;
		
	}
	void white_init() {
		white = 0x0000001008000000;
	}
	ull get_black(){	return black;	}
	ull get_white(){	return white;	}
	void print_board(ull moves);
	bool b_is_board(ull bit) {	return (black & bit) ? true : false;	}
	bool w_is_board(ull bit) {	return (white & bit) ? true : false;	}
	bool change_hint_() { return hint = not hint; }
	ull or_black(ull bit) { return black = black | bit; }
	ull or_white(ull bit) { return white = white | bit; }
	ull xor_black(ull bit) { return black = black ^ bit; }
	ull xor_white(ull bit) { return white = white ^ bit; }
};

