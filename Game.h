#pragma once

#include <vector>
#include <utility>

#include "Board.h"
class Game
{
private:
	Board board;
	bool turn;	//trueなら先手，falseなら後手
	unsigned short turn_num;
	short black_ai = 0;//0は手動,1はランダム,2は強めランダム,3は弱めランダム,4は強いAI,5は弱いAI
	short white_ai = 0;
	std::vector<short> eva{
		30, -12, 0, -1, -1, 0, -12, 30,
		-12, -15, -3, -3, -3, -3, -15, -12,
		0, -3, 0, -1, -1, 0, -3, 0,
		-1, -3, -1, -1, -1, -1, -3, -1,
		-1, -3, -1, -1, -1, -1, -3, -1,
		0, -3, 0, -1, -1, 0, -3, 0,
		-12, -15, -3, -3, -3, -3, -15, -12,
		30, -12, 0, -1, -1, 0, -12, 30 };
	short blDEPTH = 4, whDEPTH = 4, s_depth = 3;
public:
	Game();
	~Game();
	void initialize();
	Board get_board() { return board; }
	bool move(ull bit);
	ull trans_E(ull bit) {
		return (bit >> 1) & 0x7f7f7f7f7f7f7f7f;
	}
	ull trans_W(ull bit) {
		return (bit << 1) & 0xfefefefefefefefe;
	}
	ull trans_N(ull bit) {
		return (bit << 8) & 0xffffffffffffffff;
	}
	ull trans_S(ull bit) {
		return (bit >> 8);
	}
	ull trans_NE(ull bit) {
		return (bit << 7) & 0x7f7f7f7f7f7f7f7f;
	}
	ull trans_SE(ull bit) {
		return (bit >> 9) & 0x7f7f7f7f7f7f7f7f;
	}
	ull trans_SW(ull bit) {
		return (bit >> 7) & 0xfefefefefefefefe;
	}
	ull trans_NW(ull bit) {
		return (bit << 9) & 0xfefefefefefefefe;
	}
	ull reverse(ull black, ull white, ull bit);
	bool change_turn() { return turn = !turn; }
	bool get_turn() { return turn; }
	ull has_moves();
	bool is_end();
	bool change_hint() { return board.change_hint_(); }
	ull get_random_move();
	ull get_rantuyo_move(short depth);
	ull get_ranyowa_move(short depth);
	ull do_random_move() {
		ull move = get_random_move();
		if (do_move(move))
			return move;
		else
			return 0;
	}
	ull do_rantuyo_move() {
		ull move = turn ? get_rantuyo_move(blDEPTH) : get_rantuyo_move(whDEPTH);
		if (do_move(move))
			return move;
		else
			return 0;
	}
	ull do_ranyowa_move() {
		ull move = turn ? get_ranyowa_move(blDEPTH) : get_ranyowa_move(whDEPTH);
		if (do_move(move))
			return move;
		else
			return 0;
	}
	bool do_move(ull bit) {
		if (move(bit)) {
			change_turn();
			return true;
		}
		else
			return false;
	}
	int count_black() { return bitcount(board.get_black()); }
	int count_white() { return bitcount(board.get_white()); }
	short get_black_ai() { return black_ai; }
	short get_white_ai() { return white_ai; }
	int evaluate();
	short black_human() { return black_ai = 0; }
	short black_random() { return black_ai = 1; }
	short black_ran_tuyo() { return black_ai = 2; }
	short black_ran_yowa() { return black_ai = 3; }
	short black_ai_tuyo() { return black_ai = 4; }
	short black_ai_yowa() { return black_ai = 5; }
	short white_human() { return white_ai = 0; }
	short white_random() { return white_ai = 1; }
	short white_ran_tuyo() { return white_ai = 2; }
	short white_ran_yowa() { return white_ai = 3; }
	short white_ai_tuyo() { return white_ai = 4; }
	short white_ai_yowa() { return white_ai = 5; }
	std::pair<int, ull> search(short depth);
	std::pair<int, ull> search_yowa(short depth);
	std::vector<std::pair<int, ull>> search_kyoutuu(short depth);
	ull get_black() { return board.get_black(); }
	ull get_white() { return board.get_white(); }
	ull ai_tuyoi() {
		ull move = turn ? search(blDEPTH).second : search(whDEPTH).second;
		do_move(move);
		return move;
	}
	ull ai_yowai() {
		ull move = turn ? search_yowa(blDEPTH).second : search_yowa(whDEPTH).second;
		do_move(move);
		return move;
	}
	int hum_eva(ull move);
};

