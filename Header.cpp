#include "pch.h"

#include "Header.h"

short bitcount(ull bit) {
	ull bits = bit;
	bits = (bits & (ull)0x5555555555555555) + (bits >> 1 & (ull)0x5555555555555555);    //  2bitごとに計算
	bits = (bits & (ull)0x3333333333333333) + (bits >> 2 & (ull)0x3333333333333333);    //  4bitごとに計算
	bits = (bits & (ull)0x0f0f0f0f0f0f0f0f) + (bits >> 4 & (ull)0x0f0f0f0f0f0f0f0f);    //  8bitごとに計算
	bits = (bits & (ull)0x00ff00ff00ff00ff) + (bits >> 8 & (ull)0x00ff00ff00ff00ff);    //  16ビットごとに計算   
	bits = (bits & (ull)0x0000ffff0000ffff) + (bits >> 16 & (ull)0x0000ffff0000ffff);    //  32ビット分を計算   
	return (short)((bits & (ull)0x00000000ffffffff) + (bits >> 32));
}

ull get_first_1_bit(ull bit) {
	return (bit &(-(long long)bit));
}
short get_first_1_num(ull bit) {
	return bitcount((bit &(-(long long)bit)) - 1)+1;
}
