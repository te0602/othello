// Othello.cpp : このファイルには 'main' 関数が含まれています。プログラム実行の開始と終了がそこで行われます。
//

#include "pch.h"
#include <iostream>
#include <algorithm>

#include "Game.h"

using namespace std;

ull str2bit(string str) {
	char a;
	int b ;
	if (isalpha(str[0]) && isdigit(str[1])) {
		a = str[0];
		b = str[1] - '0';
	}
	else if (isalpha(str[1]) && isdigit(str[0])) {
		a = str[1];
		b = str[0] - '0';
	}
	else {
		cout << "入力がおかしいよ" << endl;
		return 0;
	}
	return (ull)num2bit(9 - (a - '@') + (8 - b) * 8);
}


int main()
{
	int evaluate = 0;
	short depth = 3;
	bool end = false;
	string input;
	Game game;
	bool bl_settai = false, wh_settai = false;
	while (!game.is_end()) {
		game.get_board().print_board(game.has_moves());
		cout << "評価値:" << game.evaluate()<<"    ";
		if (game.is_end())
			break;
		if (game.get_turn())
			cout << "〇の手番です" << endl;
		else
			cout << "●の手番です" << endl;
		if (!game.has_moves()) {
			game.change_turn();
			cout << "合法手なし" << endl;
			if (end) {
				cout << "ゲーム終了です" << endl;
				break;
			}
			else {
				end = true;
			}
		}
		else {
			end = false;
			if (game.get_turn()) {
				switch (game.get_black_ai()) {
				case 1://ランダム
					DEBUG("ランダムに着手します");
					game.do_random_move();
					continue;
				case 2://ランダム強め
					DEBUG("強めランダムに着手します");
					game.do_rantuyo_move();
					continue;
				case 3://ランダム弱め
					DEBUG("弱めランダムに着手します");
					game.do_ranyowa_move();
					continue;
				case 4://AI強い
					DEBUG("強いAIが着手します");
					game.ai_tuyoi();
					continue;
				case 5://AI弱い
					DEBUG("弱いAIが着手します");
					game.ai_yowai();
					continue;
				default:
					;
				}
			}
			else {
				switch (game.get_white_ai()) {
				case 1://ランダム
					DEBUG("ランダムに着手します");
					game.do_random_move();
					continue;
				case 2://ランダム強め
					DEBUG("強めランダムに着手します");
					game.do_rantuyo_move();
					continue;
				case 3://ランダム弱め
					DEBUG("弱めランダムに着手します");
					game.do_ranyowa_move();
					continue;
				case 4://AI強い
					DEBUG("強いAIが着手します");
					game.ai_tuyoi();
					continue;
				case 5://AI弱い
					DEBUG("弱いAIが着手します");
					game.ai_yowai();
					continue;
				default:
					;
				}
			}
			if (not(bl_settai || wh_settai)) {
				cout << "入力してください.M:手を指す.Help:操作説明" << endl;
				cin >> input;
				transform(input.begin(), input.end(), input.begin(), toupper);
				//着手
				if ((input[0]) == 'M') {
					cout << "着手を行います" << endl;
					if (input.size() >= 3) {
						game.do_move(str2bit(input.substr(1, 2)));
					}
					else
						cout << "文字数が足りません" << endl;
				}
				//ヒント表示のon/off
				else if (input == "HINT") {
					game.change_hint();
				}
				//操作方法の表示
				else if (input == "HELP") {
					cout << "Hint:合法手の表示のon/offを切り替える.Search:AIが探索した最善手を教えてくれる．Reset:初期状態に戻す." << endl;
					cout << "m([a~h][1~8]|[1~8][a~h])で指定の箇所に着手します.例:md4,m6e\nBL~で先手，WH~で後手のAIを決める．HUMで手動，RANでランダム，RANTUYOで強めのランダム，RANYOWAで弱めのランダム，AITUYOで強いAI，AIYOWAで弱いAI.\n例:BLRANで先手がランダムWHAITUYOで後手が強いAI" << endl;
					cout << "BLSETTAIで先手が人間後手が接待AI，WHSETTAIで後手が接待AI先手が人間" << endl;
				}
				//終了
				else if (input[0] == 'E' || input == "END") {
					break;
				}
				//最善手の表示
				else if (input[0] == 'S' || input == "SEARCH") {
					ull move = game.search(depth).second;
					move = get_first_1_num(move);
					char str = 'h' - ((move-1) % 8);
					cout << 'm' << str << 8 - (move-1) / 8 << endl;
				}
				//初期状態に戻す
				else if (input[0] == 'R' || input == "RESET") {
					evaluate = 0;
					end = false;
					bl_settai = false;
					wh_settai = false;
					game.initialize();
					game.black_human();
					game.white_human();
				}
				else if (input == "BLHUM")		game.black_human();
				else if (input == "BLRAN")		game.black_random();
				else if (input == "BLRANTUYO")	game.black_ran_tuyo();
				else if (input == "BLRANYOWA")	game.black_ran_yowa();
				else if (input == "BLAITUYO")   game.black_ai_tuyo();
				else if (input == "BLAIYOWA")	game.black_ai_yowa();
				else if (input == "WAHUM")		game.white_human();
				else if (input == "WHRAN")		game.white_random();
				else if (input == "WHRANTUYO")	game.white_ran_tuyo();
				else if (input == "WHRANYOWA")	game.white_ran_yowa();
				else if (input == "WHAITUYO")	game.white_ai_tuyo();
				else if (input == "WHAIYOWA")	game.white_ai_yowa();
				else if (input == "BLSETTAI") { bl_settai = true; wh_settai = false; game.black_human(); }
				else if (input == "WHSETTAI") { bl_settai = false; wh_settai = true; game.white_human(); }
				else if (input[0] == 'H') { game.change_hint(); }
			}
			else if((bl_settai && game.get_turn())||(wh_settai && !game.get_turn())){
				ull move = 0;
				cout << "入力してください.M:手を指す.Hint:合法主を表示のon/off.S:最善手を教えてもらう." << endl;
				cin >> input;
				transform(input.begin(), input.end(), input.begin(), toupper);
				if ((input[0]) == 'M') {
					cout << "着手を行います" << endl;
					if (input.size() >= 3) {
						move = str2bit(input.substr(1, 2));
						evaluate += game.hum_eva(move);
						game.do_move(move);
					}
					else
						cout << "文字数が足りません" << endl;
				}
				//ヒント表示のon/off
				else if (input[0] == 'H') {
					game.change_hint();
				}
				//評価値の高い手を教えてもらう
				else if (input[0] == 'S' || input == "SEARCH") {
					ull move = game.search(depth).second;
					move = get_first_1_num(move);
					char str = 'h' - ((move - 1) % 8);
					cout << 'm' << str << 8 - (move - 1) / 8 << endl;
				}
				//初期状態に戻す
				else if (input[0] == 'R' || input == "RESET") {
					evaluate = 0;
					end = false;
					bl_settai = false;
					wh_settai = false;
					game.initialize();
				}
				//終了
				else if (input[0] == 'E' || input == "END") {
					break;
				}
			}
			else {
				cout << "AIが探索中です" << endl;
				if (evaluate >= 15) { game.ai_tuyoi(); }
				else if (evaluate > 5) { game.do_rantuyo_move(); }
				else if (evaluate > -5) { game.do_random_move(); }
				else if (evaluate > -15) { game.do_ranyowa_move(); }
				else { game.ai_yowai(); }
			}
		}
	}
	game.get_board().print_board(game.has_moves());
	cout << "ゲーム終了" << endl;
	int black = game.count_black();
	int white = game.count_white();
	cout << "〇:" << black << "●:" << white << endl;
	if (black < white) {
		//cout << -1 << endl;
		cout << "●の勝利";
	}
	else if (black > white) {
		//cout << 1 << endl;
		cout << "〇の勝利";
	}
	else {
		//cout << 0 << endl;
		cout << "引き分け";
	}
}

// プログラムの実行: Ctrl + F5 または [デバッグ] > [デバッグなしで開始] メニュー
// プログラムのデバッグ: F5 または [デバッグ] > [デバッグの開始] メニュー

// 作業を開始するためのヒント: 
//    1. ソリューション エクスプローラー ウィンドウを使用してファイルを追加/管理します 
//   2. チーム エクスプローラー ウィンドウを使用してソース管理に接続します
//   3. 出力ウィンドウを使用して、ビルド出力とその他のメッセージを表示します
//   4. エラー一覧ウィンドウを使用してエラーを表示します
//   5. [プロジェクト] > [新しい項目の追加] と移動して新しいコード ファイルを作成するか、[プロジェクト] > [既存の項目の追加] と移動して既存のコード ファイルをプロジェクトに追加します
//   6. 後ほどこのプロジェクトを再び開く場合、[ファイル] > [開く] > [プロジェクト] と移動して .sln ファイルを選択します
