#include "pch.h"
#include <iostream>

#include "Board.h"

using namespace std;


Board::Board()
{
	initialize();
}


Board::~Board()
{
}

void Board::print_board(ull moves) {
	ull bit = 1;
	for (int i = 0; i < 63; i++) {
		bit = bit << 1;
	}
	string str = "   a b c d e f g h \n";
	str += " ------------------\n";
	for (int i = 0; i < 8; i++) {
		str += to_string(i + 1) + "|";
		for (int j = 0; j < 8; j++) {
			if (b_is_board(bit)) {
				str += "�Z";
			}
			else if (w_is_board(bit)) {
				str += "��";
			}
			else if (hint && (moves&bit)) {
				str += "��";
			}
			else {
				str += "��";
			}
			bit = bit >> 1;
		}
		str += "|\n";
	}
	str += " ------------------\n";
	cout << str << endl;
}


