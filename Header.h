#pragma once


#define ull unsigned long long
#include <bitset>

#define num2bit(num) (ull)((ull)1 << ( num - 1))
#define print_bit(num) (cout << bitset<64>(num)<<endl)
#define DEBUG(str) if(debug)cout << str << endl 

short bitcount(ull bit);
ull get_first_1_bit(ull bit);
short get_first_1_num(ull bit);

static const bool debug = false;